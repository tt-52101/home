# 聚BT - 聚合最优质的BT、磁力资源   
### 请Ctrl+D收藏本网页地址，确保永久访问  

| 地址       | 类型  | 是否需要翻墙 |  
| :---       |     :---:      |          ---: |
| [jubt.gq](https://jubt.gq)    | 最新地址 | 不需要 |  
| [jubt.cf](https://jubt.cf)    | 最新地址 | 不需要 |  
| [jubt.me](https://jubt.me)    | 最新地址 | 不需要 |  
| [jubt.net](https://jubt.net) | 最新地址 | 需要翻墙 |  


支持邮箱：[support@jubt.net](mailto:support@jubt.net)  

Twitter：[@jubtnet8](https://twitter.com/jubt8)  
  
Weibo：[@jubt8](https://weibo.com/jubt8)  
  
防屏蔽方法：[Chrome/Firefox配置DoH，解决DNS劫持问题](https://www.yeeach.com/post/1507)  






